---
title: Blog van week 1
subtitle: Het begin van de Design Challenge
date: 2017-09-11
tags: ["blog", "blogging", "design"]
---

# Blog – Design Challenge 1

## Maandag 4 september 2017
Ons team bestaat uit vijf mensen: Samantha, Lindsay, Chanouk, Jascha en ik (Xander). We moeten deze periode een game gaan ontwerpen. De opdracht luidt:

> Ontwerp een game, die gebruik maakt van een online interactief element, die in de introweek de studenten onderling en met de stad verbindt.

Vandaag hebben we onze teamregels opgesteld. Ik ben gekozen als teamcaptain van onze groep. Ook hebben we ruwweg bepaald wat onze doelgroep wordt. We houden het voorlopig op *eerstejaars kunststudent*.

Ieder lid van ons team onderzoekt een specifieke opleiding binnen deze groep. Ikzelf onderzocht *film*. Rotterdam is echter niet echt filmstad. Veel informatie heb ik dus niet kunnen vinden.

## Dinsdag 5 september 2017
Vandaag hebben we een hoorcollege gehad over designtheorie van Raul. Er is uitgelegd wat design precies inhoudt en wat het verschil is tussen design en kunst. Eén zin in het bijzonder is me bijgebleven:

> **Design** is to **design** a **design** to produce a **design**.

Vervolgens hebben we, nadat we onze onderzoeken naar specifieke opleidingen binnen de kunstsector hadden vergeleken, gekozen om *eerstejaars fotografiestudent* als doelgroep voor onze game te nemen.

In de mediatheek van de Willem de Kooning-academie is er een Google Drive aangemaakt en ben ik begonnen aan een moodboard voor deze groep. Ook heb ik wat vragen verzonnen voor het interview dat we willen houden met mensen van deze doelgroep.

## Woensdag 6 september 2017
In de ochtend moest ik om 9:00 uur op school zijn met alle teamcaptains. We hielden een stand-upmeeting. Al staand gingen we iedereen langs om de voortgang per team te peilen. Ik gaf aan dat het ging ging met het team, maar dat er misschien wat opstartproblemen waren qua communicatie. Bob heeft me hierover tips gegeven. *Extreme "programming"*, een methode waar je met z'n tweeën aan één computer werkt, was daar één van.

Daarna hebben we in de klas, na wat opstarttijd, onze moodboards afgemaakt. Mijn moodboard werd goedgekeurd. Ook heb ik geopperd om onze planning aan te scherpen, zodat iedereen altijd weet wat ie moet doen.

Na een uurtje pauze (en een patatje curry) hebben we meer informatie gekregen over hoe de blog gemaakt moet worden. Ik moet de blog gaan schrijven in *Markdown*. Dit is een simpele codeertaal. Hier heb ik het programma *MacDown* voor gedownload.

Toen de uitleg was afgelopen en ik mijn blog had bijgewerkt, heb ik de planning in elkaar gezet. Iedereen weet nu wat ie moet doen t/m maandag 9 september. Hierna heb ik samen met Chanouk het interview/de survey afgemaakt en online gezet.

## Donderdag 7 september 2017
Ik heb in de ochtend contact gehad met een vriendin van mij die sinds een week fotografie studeert aan de WdKA. Zij vertelde mij dat ze nog niet mee kon doen met het interview, aangezien haar opleiding op dit moment op excursie is met school.

Om 13:00 uur begon de les. Het was een werkcollege *design theorie*. We moesten ieder apart het ontwerpproces vastleggen op een A3 met een zelfverzonnen opdracht. Ik koos als opdracht het 'ontwerpen' van mijn housewarming uit januari. Na vijftien minuten was de tijd hiervoor voorbij en gingen we elkaar feedback geven.

Hierna kozen we de opdracht van Chanouk uit, aangezien die het best uitgewerkt was. Deze heb ik toen gevisualiseerd op een A3 en na een halfuur hebben we deze gezamelijk gepresenteerd voor de klas.

Eigenlijk wilde ik een manier verzinnen om na schooltijd alsnog het interview te houden (desnoods met tweedejaarsstudenten), maar drie mensen uit het team konden niet, dus besloot ik *The Sketchnote Handbook* van Mike Rohde te bestuderen en er aantekeningen van te maken. Dit heb ik van halfdrie tot zes uur gedaan in de mediatheek van WdKA. In het boek wordt uitgebreid uitgelegd hoe je visuele aantekeningen kan maken op een goede manier. Dit schijnt te helpen bij het verwerken en onthouden van informatie, omdat je meerdere zintuigen tegelijk gebruikt.